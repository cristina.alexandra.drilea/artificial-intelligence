from random import shuffle
from model import NeuralNetwork
import torch
import torchvision
from torchvision import datasets, transforms
from torch import nn, optim
import matplotlib.pyplot as plt

if __name__ == "__main__":
    # Instantiate the neural network
    model = NeuralNetwork()

    # Hyperparameters
    epochs = 10
    batch_size_train = 32
    learning_rate = 0.003

    ##### Descarcam setul de date MNIST din libraria torchvision sub forma unui obiect de tipul Dataset
    ##### Vom folosi un obicet de tip DataLoader care primest ca si argument obiectul Dataset

    # Preprocesing steps
    transform = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])

    # Datasets
    # train_dataset = torchvision.datasets.CIFAR10('./files', train=True, download=True, transform=transform)
    # validation_dataset = torchvision.datasets.CIFAR10('./files', train=False, download=True, transform=transform)
    train_dataset = torchvision.datasets.MNIST('./files', train=True, download=True, transform=transform)
    validation_dataset = torchvision.datasets.MNIST('./files', train=False, download=True, transform=transform)

    # Dataloaders
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size_train, shuffle=True)
    validation_loader = torch.utils.data.DataLoader(validation_dataset, batch_size=1, shuffle=True)

    # Defining loss function NegativeLogLikeliHood function
    criterion = nn.NLLLoss()

    # Definirea optimizatorului, Stochastic Gradient Descend
    optimizer = optim.SGD(model.parameters(), lr=learning_rate)

    ####  Pipepline pentru antrenare si testare
    # Definim 2 liste in care vom pastra erorile aferente fiecarei epoci
    errors_train = []
    errors_validation = []

    # Training loop
    for epoch in range(epochs):
        # Lista in care vom stoca erorile temporare epocii curente
        temporal_loss_train = []

        # Functiia .train() trebuie apelata explicit inainte de antrenare
        model.train()

        #Iteram prin toate sample-urile generate de dataloader
        for images, labels in train_loader:
            # Flatten MNIST images from shape (28,28) into a (784, ) long vector. Thie first axis is always the batch. Here -1 means 
            # "take all the other dimensions and flatten them"
            # As a results from a tensor with shape (batch_size, 28, 28) we will obtain a tensor with shape (batch_size, 784)
            images = images.view(images.shape[0], -1)

            # Clean the gradients
            optimizer.zero_grad()

            # Forward propagation
            output = model(images)

            # Compute the error
            loss = criterion(output, labels)
            temporal_loss_train.append(loss.item()) 

            # Backpropagation (computing the gradients for each weight)
            loss.backward()

            # Update the weights
            optimizer.step()

# Now, after each epoch, we have to see how the model is performinh on the validation set
# Before, evaluation we have to explicity call .eval() method

        model.eval()
        temporal_loss_valid = []
        for images, labels in validation_loader:
            # Same flattening as above
            images = images.view(images.shape[0], -1)

            # Forward pass
            output = model(images)

            # Compute the error
            loss = criterion(output, labels)
            temporal_loss_valid.append(loss.item())

        # Compute metrics after each epoch (mean value of loss)
        medium_epoch_loss_train = sum(temporal_loss_train)/len(temporal_loss_train)
        medium_epoch_loss_valid = sum(temporal_loss_valid)/len(temporal_loss_valid)

        errors_train.append(medium_epoch_loss_train)
        errors_validation.append(medium_epoch_loss_valid)

        print(f"Epoch {epoch}. Training loss: {medium_epoch_loss_train}. Validations loss: {medium_epoch_loss_valid}")

plt.title("Learning curves")
print(errors_train)
plt.plot(errors_train, label='Training loss')
plt.plot(errors_validation, label='Validation loss')
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.legend()
plt.show()