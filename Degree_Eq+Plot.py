import math
import numpy as np
import matplotlib.pyplot as plt

def degree_eq(a, b, c):
    delta =  a*a - 4*a*c
    if delta >= 0:
        x1 = (-b + math.sqrt(delta)) / 2*a
        x2 = (-b - math.sqrt(delta)) / 2*a
        return x1, x2
    else:
        print(f"Delta {delta} este negativ")

a=-5 
b=14
c=3
s = degree_eq(a, b, c)

x = np.array(range (0,6))
y = np.multiply(a, x**2) + np.multiply(b, x) + c

y_max = np.amax(y)
#Timpul la care se atinge inaltimea maxima
x_max = x[np.argmax(y)]

print(f"Mingea este la inaltimea maxima in ({y_max}, {x_max})")

plt.plot(x, y, 'g*--')
plt.xlabel("Time")
plt.ylabel("Height")
plt.show()
