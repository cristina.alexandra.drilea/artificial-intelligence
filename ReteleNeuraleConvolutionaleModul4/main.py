# Imports
import numpy as np
import torch
import torchvision
import matplotlib.pyplot as plt
from time import time
from torchvision import datasets, transforms
from torch import nn, optim
from model import NeuralNetwork

if __name__ == "__main__":
    # Instantiate the neural network
    model = NeuralNetwork()

    # For reproductibility
    random_seed = 1
    torch.backends.cudnn.enabled = False
    torch.manual_seed(random_seed)

    # Hyperparameters
    epochs = 5
    batch_size_train = 32
    learning_rate = 0.03

    # Preprocessing steps 
    transform = transforms.Compose([transforms.ToTensor()])

    # Datasets
    ############ FOR MNIST ############
    # train_dataset = torchvision.datasets.MNIST('./files/', train=True, download=True, transform=transform)
    # validation_dataset =  torchvision.datasets.MNIST('./files/', train=False, download=True, transform=transform)

    train_dataset = torchvision.datasets.CIFAR10('./files/', train=True, download=True, transform=transform)
    validation_dataset =  torchvision.datasets.CIFAR10('./files/', train=False, download=True, transform=transform)

    # Dataloaders
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size_train, shuffle=True)
    validation_loader = torch.utils.data.DataLoader(validation_dataset, batch_size=1, shuffle=False)

    # Definirea loss-ului, functia NegativeLogLikeliHood
    criterion = nn.NLLLoss()

    # Definirea optimizatorului, in cazul nostru Stochastic Gradient Descend
    optimizer = optim.SGD(model.parameters(), lr=learning_rate)

    # Pipeline pentru antrenare si testare #

    # Doua liste in care vom pastra erorile aferente fiecarei epoci
    errors_train = []
    errors_validation = []

    # Training loop
    for epoch in range(epochs):
        # O lista unde vom stoca erorile temporare epocii curente
        temporal_loss_train = [] 
        
        # Functia .train() trebuie apelata explicit inainte de antrenare
        model.train() 
        
        # Iteram prin toate sample-urile generate de dataloader
        for images, labels in train_loader:        
            # Clean the gradients
            optimizer.zero_grad()
            
            # Forward propagation
            output = model(images)

            # Compute the error
            loss = criterion(output, labels)
            temporal_loss_train.append(loss.item())
            
            # Backpropagation (computing the gradients for each weight)
            loss.backward()
            
            # Update the weights
            optimizer.step()
        
        # Now, after each epoch, we have to see how the model is performing on the validation set #
        # Before evaluation we have to explicitly call .eval() method
        model.eval()
        temporal_loss_valid = []
        for images, labels in validation_loader:
            # Forward pass
            output = model(images)
        
            # Compute the error
            loss = criterion(output, labels)
            temporal_loss_valid.append(loss.item())
            
        # Compute metrics after each epoch (mean value of loss) #
        medium_epoch_loss_train = sum(temporal_loss_train)/len(temporal_loss_train)
        medium_epoch_loss_valid = sum(temporal_loss_valid)/len(temporal_loss_valid)

        errors_train.append(medium_epoch_loss_train)
        errors_validation.append(medium_epoch_loss_valid)

        print(f"Epoch {epoch}. Training loss: {medium_epoch_loss_train}. Validation loss: {medium_epoch_loss_valid}")

plt.title("Learning curves")
plt.plot(errors_train, label='Training loss')
plt.plot(errors_validation, label='Validation loss')
plt.xlabel("Epoch")
plt.ylabel('Loss')
plt.legend()
plt.show()