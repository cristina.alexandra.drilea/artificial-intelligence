# Definirea modelului
from torch import nn

class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()

        # Layers
        ############ FOR MNIST ############
        # self.conv_layer1 = nn.Conv2d(1, 16, 3, padding=1)
        # self.conv_layer2 = nn.Conv2d(16, 16, 3, padding=1)
        # self.conv_layer3 = nn.Conv2d(16, 32, 3, padding=1)
        # self.pooling = nn.MaxPool2d(2)

        # self.final_layer1 = nn.Linear(288, 128)
        # self.final_layer2 = nn.Linear(128, 10)

        self.conv_layer1 = nn.Conv2d(3, 16, 3, padding=1)
        self.conv_layer2 = nn.Conv2d(16, 32, 3, padding=1)
        self.conv_layer3 = nn.Conv2d(32, 64, 3, padding=1)
        self.pooling = nn.MaxPool2d(2)

        self.final_layer1 = nn.Linear(1024, 512)
        self.final_layer2 = nn.Linear(512, 64)
        self.final_layer3 = nn.Linear(64, 10)


        # Activations
        self.activation_hidden = nn.LeakyReLU()
        self.activation_final = nn.LogSoftmax(dim=1)

        # Dropout
        ############ FOR MNIST ############
        # self.drop = nn.Dropout(0.3)
        self.drop = nn.Dropout(0.5)

    def forward(self, x):
        print(f"Shape tensor care intra in retea: {x.shape}")
        # Feature extraction
        x = self.conv_layer1(x)
        x = self.activation_hidden(x)
        x = self.pooling(x)
        print(f"Shape-ul dupa primul block de convolutie: {x.shape}")

        x = self.conv_layer2(x)
        x = self.activation_hidden(x)
        x = self.pooling(x)
        print(f"Shape-ul dupa al doilea block de convolutie: {x.shape}")
        
        x = self.conv_layer3(x)
        x = self.activation_hidden(x)
        x = self.pooling(x)
        print(f"Shape-ul dupa al treilea block de convolutie: {x.shape}")

        x = self.conv_layer4(x)
        x = self.activation_hidden(x)
        x = self.pooling(x)
        print(f"Shape-ul dupa al patrulea block de convolutie: {x.shape}")

        # Classification
       # x = x.view(x.shape[0], -1)
        x = x.view(-1, 1024)
        print(f"Shape dupa flatteting: {x.shape}")

        x = self.final_layer1(x)
        x = self.activation_hidden(x)
        x = self.drop(x)
        print(f"Shape dupa linear layer 1: {x.shape}")

        x = self.final_layer2(x)
        x = self.activation_hidden(x)
        x = self.drop(x)
        print(f"Shape dupa linear layer 2: {x.shape}")

        x = self.final_layer3(x)
        x = self.activation_final(x)
        print(f"Shape dupa linear layer 5: {x.shape}")
        print("-"*30)

        return x
