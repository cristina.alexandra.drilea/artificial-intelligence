import os
import random
import json

if __name__ == "__main__":
    # Input data
    current_dir = os.getcwd()
    dataset_dir = 'Data/face_dataset'
    dataset_path = os.path.join(current_dir, dataset_dir)
    print(f"Your dataset {dataset_path}")
    split_ratio = [0.7, 0.15, 0.15]

    # Dict for storing the splits 
    split = {"train": [], "test": [], "val": []}

    print('ceva')

    # Iterate throught folders
    for class_folder in os.listdir(dataset_path):
        print(f"class folder is {class_folder}")
        class_folder_path = os.path.join(dataset_path, class_folder)
        print(class_folder_path)
        
        # All iamges from current class
        all_images = os.listdir(class_folder_path)

        train_idx = int(split_ratio[0] * len(os.listdir(class_folder_path))) 
        val_idx = int(split_ratio[1] * len(os.listdir(class_folder_path))) 
        test_idx= int(split_ratio[2] * len(os.listdir(class_folder_path))) 

        # Shuffle the list
        random.shuffle(all_images)

        # Remove extenstions
        all_images = [x.replace(".png", "") for x in all_images]

        # Split the list
        train_temp = all_images[0:train_idx]
        val_temp = all_images[train_idx: train_idx + val_idx]
        test_temp = all_images[train_idx + val_idx:]

        # Append to main dictionary
        split['train'].extend(train_temp)
        split['val'].extend(val_temp)
        split['test'].extend(test_temp)

    # Save json with splits
    with open('split.json', 'w') as f:
        json.dump(split, f)