from torch import nn

class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()
        # Layers
        self.layer1 = nn.Linear(784, 128)
        self.layer2 = nn.Linear(128, 64)
        self.layer3 = nn.Linear(64, 10)
        
        #Activations
        self.activation_hidden = nn.ReLU()
        self.activation_final = nn.LogSoftmax(dim=1)

    def forward(self, x):
        x = self.layer1(x)
        x = self.activation_hidden(x)

        x = self.layer2(x)
        x = self.activation_hidden(x)

        x = self.layer3(x)
        x= self.activation_final(x)

        return x



